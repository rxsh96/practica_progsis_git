#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define MAX_STR 100 

float kgtolb(float);
char *cmtoft(int);

int main(){
	
	system("clear");
	
	FILE *fp, *nfp;
	char filename[MAX_STR];
	char nombre[MAX_STR];
	char apellido[MAX_STR];
	int edad, estatura;
	float peso;
	
	printf("Ingrese nommbre archivo: ");
	scanf("%s", filename);

	int n;
	
	if((fp = fopen(filename, "r")) != NULL){
		nfp = fopen("datosImperiales.txt", "w");
		do{
			n = fscanf(fp,"%s\t%s\t%d\t%f\t%d*", nombre, apellido, &edad, &peso, &estatura);
			
			float lbweight = kgtolb(peso);
			char *ftHeight = cmtoft(estatura);

			if(n > 4){
				printf("El integrante %s %s tiene %d años, pesa %.2f libras y mide %s.\n", nombre, apellido, edad, lbweight, ftHeight);
				fprintf(nfp,"%s\t%s\t%d\t%.2f\t%s\n", nombre, apellido, edad, lbweight, ftHeight);
			}
		}
		while(n > 0);
		fclose(fp);
		fclose(nfp);
		printf("\nSe ha creado el archivo \"Datos Imperiales\" con la informacion en unidades Imperiales.\n");
	}
	else{
		printf("El archivo no existe!\n");
	}
}


float kgtolb(float kgweight){
	return kgweight * 2.202;
}


char *cmtoft(int cmheight){
	int ft =  cmheight/30.48;
	int remainder = (int)fmod(cmheight, 30.48);
	int inches = remainder/2.54;
	char *str = (char *) malloc(10);
	sprintf(str, "%d\"%d'", ft, inches);
	return str;
}
